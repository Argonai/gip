"""
Routes and views for the flask application.
"""

from datetime import datetime

import bcrypt
from flask import render_template, request, session, redirect, url_for, make_response
from GIP_redone import app
import mysql.connector

con = mysql.connector.connect(user='website', password='Umpj5PT9p2X4m6',
                              host='127.0.0.1',
                              database='users')
cur = con.cursor()
@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )

@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )
@app.route('/login', methods=['GET','POST'])
def login():
    cur = con.cursor(buffered=True)
    resp = make_response(render_template('login.html'))
    
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password'].encode('utf-8')
        cur.execute('SELECT hashed FROM users WHERE email = "{}"'.format(email))
        for i in cur:
            hashed_pw = i
        
        pw_correct = bcrypt.checkpw(password, bytes(hashed_pw[0]))
        if pw_correct:
            cur.execute('SELECT id from users where email = "{}"'.format(email))
            for i in cur:
                user = bytes(i[0])
            #user = int.from_bytes(temp_var, 'big')
            resp.set_cookie('userID', user)
            cur.close()
        return resp
    return resp
    
@app.route('/register', methods=['GET','POST'])
def register():
    cur = con.cursor(buffered=True)
    if request.method == 'POST':
        
        originalpass = request.form['password']
        #originalpass = bytes(originalpass, "utf-8")
        doublepass = request.form['passwordcheck']
        #doublepass = bytes(doublepass, "utf-8")
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        if originalpass == doublepass:
            salt = bcrypt.gensalt()
            hashed = bcrypt.hashpw(originalpass.encode("utf-8"), salt)
            hashed = hashed.decode('utf-8')
            cur.execute('INSERT into users (firstname, lastname, email, hashed, salt) VALUES ("{}","{}","{}","{}","{}")'.format(firstname, lastname, email, hashed, salt))
            con.commit()
            cur.close()
        return render_template('register.html')
    return render_template('register.html')
    